<?php

use App\Domain\Categories\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('electrons', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Category::class)
                ->index()
                ->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('author');
            $table->text('title');
            $table->text('description')->nullable();
            $table->integer('year_publication');
            $table->string('page_count')->nullable();
            $table->string('image')->nullable()->comment('books images (jpg,png,jpeg)');
            $table->string('file')->nullable()->comment('books files (pdf)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('electrons');
    }
};
