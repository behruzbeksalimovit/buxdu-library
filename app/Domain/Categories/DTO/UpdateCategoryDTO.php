<?php

namespace App\Domain\Categories\DTO;

use App\Domain\Categories\Models\Category;

class UpdateCategoryDTO
{
    /**
     * @var int|null
     */
    private ?int $parent_id = null;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var Category
     */
    private Category $category;

    /**
     * @param array $data
     * @return UpdateCategoryDTO
     */
    public static function fromArray(array $data): UpdateCategoryDTO
    {
        $dto = new self();
        $dto->setParentId($data['parent_id'] ?? null);
        $dto->setName($data['name']);
        $dto->setCategory($data['category']);

        return $dto;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @param int|null $parent_id
     */
    public function setParentId(?int $parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }
}
