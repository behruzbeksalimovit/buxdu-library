<?php

namespace App\Domain\Categories\Repositories;

use App\Domain\Categories\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {
        return Category::query()
            ->with('category')
            ->orderByDesc('id')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAllParentNull(): Collection|array
    {
        return Category::query()
            ->where('parent_id',null)
            ->get();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Category::query()
            ->orderByDesc('id')
            ->get();
    }
}
