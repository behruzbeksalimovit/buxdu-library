<?php

namespace App\Domain\Categories\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $perPage = 20;

    public function category()
    {
        return $this->hasOne(Category::class,'id','parent_id')->select('id','parent_id','name');
    }

    public function categories()
    {
        return $this->hasMany(Category::class,'parent_id','id');
    }
}
