<?php

namespace App\Domain\Electrons\Actions;

use App\Domain\Electrons\DTO\StoreElectronDTO;
use App\Domain\Electrons\DTO\UpdateElectronDTO;
use App\Domain\Electrons\Models\Electron;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UpdateElectronAction
{
    public function execute(UpdateElectronDTO $dto)
    {
        DB::beginTransaction();
        try {
            $electron = $dto->getElectron();
            $electron->category_id = $dto->getCategoryId();
            $electron->author = $dto->getAuthor();
            $electron->title = $dto->getTitle();
            $electron->description = $dto->getDescription() ?? $dto->getElectron()->description;
            $electron->year_publication = $dto->getYearPublication();
            $electron->page_count = $dto->getPageCount() ?? $dto->getElectron()->page_count;

            if ($dto->getImage() !== null) {
                File::delete('storage/electrons/images/' . $dto->getElectron()->image);
                $image = $dto->getImage();
                $imageName = Str::random(4) . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/electrons/images/' . now()->format('Y-m'), $imageName);
                $electron->image = now()->format('Y-m') . '/' . $imageName;
            }

            if ($dto->getFile() !== null) {
                File::delete('storage/electrons/files/' . $dto->getElectron()->file);
                $file = $dto->getFile();
                $filename = Str::random(4) . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/electrons/files/' . now()->format('Y-m'), $filename);
                $electron->file = now()->format('Y-m') . '/' . $filename;
            }
            $electron->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $electron;
    }
}
