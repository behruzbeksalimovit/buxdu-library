<?php

namespace App\Domain\Electrons\Actions;

use App\Domain\Electrons\DTO\StoreElectronDTO;
use App\Domain\Electrons\Models\Electron;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StoreElectronAction
{
    public function execute(StoreElectronDTO $dto)
    {
        DB::beginTransaction();
        try {
            $electron = new Electron();
            $electron->category_id = $dto->getCategoryId();
            $electron->author = $dto->getAuthor();
            $electron->title = $dto->getTitle();
            $electron->description = $dto->getDescription();
            $electron->year_publication = $dto->getYearPublication();
            $electron->page_count = $dto->getPageCount();

            if ($dto->getImage() !== null) {
                $image = $dto->getImage();
                $imageName = Str::random(4) . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/electrons/images/' . now()->format('Y-m'), $imageName);
                $electron->image = now()->format('Y-m') . '/' . $imageName;
            }

            if ($dto->getFile() !== null) {
                $file = $dto->getFile();
                $filename = Str::random(4) . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/electrons/files/' . now()->format('Y-m'), $filename);
                $electron->file = now()->format('Y-m') . '/' . $filename;
            }
            $electron->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $electron;
    }
}
