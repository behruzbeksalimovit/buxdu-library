<?php

namespace App\Domain\Electrons\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreElectronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required',
            'title' => 'required|string',
            'author' => 'required|string',
            'description' => 'nullable|string',
            'year_publication' => 'nullable',
            'page_count' => 'string',
            'image' => 'nullable|mimes:jpg,png,jpeg|max:10240',
            'file' => 'nullable|mimes:zip,pdf,rar|max:819200',
        ];
    }
}
