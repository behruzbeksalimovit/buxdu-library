<?php

namespace App\Domain\Electrons\Models;

use App\Domain\Categories\Models\Category;
use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Electron extends Model
{
    use HasFactory, Filterable;

    protected $perPage = 20;

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
