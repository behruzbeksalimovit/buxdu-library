<?php
namespace App\Domain\Electrons\Repositories;

use App\Domain\Electrons\Models\Electron;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ElectronRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {
        return Electron::query()
            ->orderByDesc('id')
            ->paginate();
    }

//    API

    /**
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function getAllBookPaginate($filter): LengthAwarePaginator
    {
        return Electron::query()
            ->Filter($filter)
            ->with('category')
            ->orderByDesc('id')
            ->paginate(30);
    }

    /**
     * @return Builder|Collection|Model|Builder[]
     */
    public function showBook($book_id): Builder|array|Collection|Model
    {
        return Electron::query()
            ->with('category')
            ->find($book_id);
    }
}
