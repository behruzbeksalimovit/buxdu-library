<?php

namespace App\Domain\Electrons\DTO;

use Illuminate\Http\UploadedFile;

class StoreElectronDTO
{
    private int $category_id;
    private string $author;
    private string $title;
    private ?string $description = null;
    private int $year_publication;
    private ?string $page_count = null;
    private ?UploadedFile $image = null;
    private ?UploadedFile $file = null;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setCategoryId($data['category_id']);
        $dto->setAuthor($data['author']);
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description'] ?? null);
        $dto->setYearPublication($data['year_publication']);
        $dto->setPageCount($data['page_count'] ?? null);
        $dto->setImage($data['image'] ?? null);
        $dto->setFile($data['file'] ?? null);

        return $dto;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getYearPublication(): int
    {
        return $this->year_publication;
    }

    /**
     * @param int $year_publication
     */
    public function setYearPublication(int $year_publication): void
    {
        $this->year_publication = $year_publication;
    }

    /**
     * @return string|null
     */
    public function getPageCount(): ?string
    {
        return $this->page_count;
    }

    /**
     * @param string|null $page_count
     */
    public function setPageCount(?string $page_count): void
    {
        $this->page_count = $page_count;
    }

    /**
     * @return UploadedFile|null
     */
    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    /**
     * @param UploadedFile|null $image
     */
    public function setImage(?UploadedFile $image): void
    {
        $this->image = $image;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }
}
