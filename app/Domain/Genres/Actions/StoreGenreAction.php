<?php

namespace App\Domain\Genres\Actions;

use App\Domain\Genres\DTO\StoreGenreDTO;
use App\Domain\Genres\Models\Genre;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreGenreAction
{
    /**
     * @param StoreGenreDTO $dto
     * @return Genre
     * @throws Exception
     */
    public function execute(StoreGenreDTO $dto): Genre
    {
        DB::beginTransaction();
        try {
            $genre = new Genre();
            $genre->name = $dto->getName();
            $genre->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $genre;
    }
}
