<?php

namespace App\Domain\Genres\Actions;

use App\Domain\Genres\DTO\StoreGenreDTO;
use App\Domain\Genres\DTO\UpdateGenreDTO;
use App\Domain\Genres\Models\Genre;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateGenreAction
{
    /**
     * @param UpdateGenreDTO $dto
     * @return Genre
     * @throws Exception
     */
    public function execute(UpdateGenreDTO $dto): Genre
    {
        DB::beginTransaction();
        try {
            $genre = $dto->getGenre();
            $genre->name = $dto->getName();
            $genre->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $genre;
    }
}
