<?php
namespace App\Domain\Genres\DTO;

class StoreGenreDTO
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @param array $data
     * @return StoreGenreDTO
     */
    public static function fromArray(array $data): StoreGenreDTO
    {
        $dto = new self();
        $dto->setName($data['name']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
