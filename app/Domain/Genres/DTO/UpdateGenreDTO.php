<?php
namespace App\Domain\Genres\DTO;
use App\Domain\Genres\Models\Genre;

class UpdateGenreDTO
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var Genre
     */
    private Genre $genre;

    /**
     * @param array $data
     * @return UpdateGenreDTO
     */
    public static function fromArray(array $data): UpdateGenreDTO
    {
        $dto = new self();
        $dto->setName($data['name']);
        $dto->setGenre($data['genre']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Genre
     */
    public function getGenre(): Genre
    {
        return $this->genre;
    }

    /**
     * @param Genre $genre
     */
    public function setGenre(Genre $genre): void
    {
        $this->genre = $genre;
    }
}
