<?php

namespace App\Domain\Genres\Models;

use App\Domain\Books\Models\Book;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $perPage = 20;

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
