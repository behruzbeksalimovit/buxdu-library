<?php

namespace App\Domain\Books\Actions;

use App\Domain\Books\DTO\StoreBookDTO;
use App\Domain\Books\Models\Book;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StoreBookAction
{
    /**
     * @param StoreBookDTO $dto
     * @return Book
     * @throws Exception
     */
    public function execute(StoreBookDTO $dto): Book
    {
        DB::beginTransaction();
        try {
            $book = new Book();
            $book->category_id = $dto->getCategoryId();
            $book->title = $dto->getTitle();
            $book->author = $dto->getAuthor();
            $book->description = $dto->getDescription();

            if ($dto->getImage() !== null) {
                $image = $dto->getImage();
                $imageName = Str::random(4) . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/books/images/' . now()->format('Y-m'), $imageName);
                $book->image = now()->format('Y-m') . '/' . $imageName;
            }

            if ($dto->getFile() !== null) {
                $file = $dto->getFile();
                $filename = Str::random(4) . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/books/files/' . now()->format('Y-m'), $filename);
                $book->file = now()->format('Y-m') . '/' . $filename;
            }
            $book->save();
            $book->genres()->sync($dto->getGenreId());
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $book;
    }
}
