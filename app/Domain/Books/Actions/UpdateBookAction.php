<?php

namespace App\Domain\Books\Actions;

use App\Domain\Books\DTO\UpdateBookDTO;
use App\Domain\Books\Models\Book;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UpdateBookAction
{
    /**
     * @param UpdateBookDTO $dto
     * @return Book
     * @throws Exception
     */
    public function execute(UpdateBookDTO $dto): Book
    {
        DB::beginTransaction();
        try {
            $book = $dto->getBook();
            $book->category_id = $dto->getCategoryId();
            $book->title = $dto->getTitle();
            $book->author = $dto->getAuthor();
            $book->description = $dto->getDescription();

            if ($dto->getImage() !== null) {
                File::delete('storage/books/images/' . $dto->getBook()->image);
                $image = $dto->getImage();
                $imageName = Str::random(4) . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/books/images/' . now()->format('Y-m'), $imageName);
                $book->image = now()->format('Y-m') . '/' . $imageName;
            }

            if ($dto->getFile() !== null) {
                File::delete('storage/books/files/' . $dto->getBook()->file);
                $file = $dto->getFile();
                $filename = Str::random(4) . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/books/files/' . now()->format('Y-m'), $filename);
                $book->file = now()->format('Y-m') . '/' . $filename;;
            }
            $book->update();
            $book->genres()->sync($dto->getGenreId());
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $book;
    }
}
