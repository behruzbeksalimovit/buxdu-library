<?php

namespace App\Domain\Books\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required',
            'genre_id' => 'required',
            'title' => 'required|string',
            'author' => 'required|string',
            'description' => 'nullable|string',
            'image' => 'nullable|mimes:jpg,png,jpeg|max:10240',
            'file' => 'nullable|mimes:pdf|max:819200',
        ];
    }
}
