<?php

namespace App\Domain\Books\Models;

use App\Domain\Categories\Models\Category;
use App\Domain\Genres\Models\Genre;
use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory, Filterable;

    protected $perPage = 20;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }
}
