<?php
namespace App\Domain\Books\Repositories;

use App\Domain\Books\Models\Book;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BookRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {
        return Book::query()
            ->with('genres')
            ->orderByDesc('id')
            ->paginate();
    }

//    API

    /**
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function getAllBookPaginate($filter): LengthAwarePaginator
    {
        return Book::query()
            ->Filter($filter)
            ->with('genres','category')
            ->orderByDesc('id')
            ->paginate(30);
    }

    /**
     * @return Builder|Collection|Model|Builder[]
     */
    public function showBook($book_id): Builder|array|Collection|Model
    {
        return Book::query()
            ->with('genres','category')
            ->find($book_id);
    }

    /**
     * @param $category_id
     * @return Builder[]|Collection
     */
    public function showForCategoryAllBooks($category_id): Collection|array
    {
        return Book::query()
            ->with('genres','category')
            ->where('category_id',$category_id)
            ->get();
    }
}
