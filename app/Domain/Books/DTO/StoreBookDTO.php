<?php

namespace App\Domain\Books\DTO;

use Illuminate\Http\UploadedFile;

class StoreBookDTO
{
    /**
     * @var int
     */
    private int $category_id;

    /**
     * @var array
     */
    private array $genre_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $author;

    /**
     * @var string|null
     */
    private ?string $description = null;

    /**
     * @var UploadedFile|null
     */
    private ?UploadedFile $image = null;

    /**
     * @var UploadedFile|null
     */
    private ?UploadedFile $file = null;

    /**
     * @param array $data
     * @return StoreBookDTO
     */
    public static function fromArray(array $data): StoreBookDTO
    {
        $dto = new self();
        $dto->setCategoryId($data['category_id']);
        $dto->setGenreId($data['genre_id']);
        $dto->setTitle($data['title']);
        $dto->setAuthor($data['author']);
        $dto->setDescription($data['description'] ?? null);
        $dto->setImage($data['image'] ?? null);
        $dto->setFile($data['file'] ?? null);

        return $dto;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return array
     */
    public function getGenreId(): array
    {
        return $this->genre_id;
    }

    /**
     * @param array $genre_id
     */
    public function setGenreId(array $genre_id): void
    {
        $this->genre_id = $genre_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return UploadedFile|null
     */
    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    /**
     * @param UploadedFile|null $image
     */
    public function setImage(?UploadedFile $image): void
    {
        $this->image = $image;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }
}
