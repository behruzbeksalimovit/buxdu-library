<?php

namespace App\Domain\Announcements\Actions;

use App\Domain\Announcements\DTO\StoreAnnouncementDTO;
use App\Domain\Announcements\DTO\UpdateAnnouncementDTO;
use App\Domain\Announcements\Models\Announcement;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UpdateAnnouncementAction
{
    public function execute(UpdateAnnouncementDTO $dto)
    {
        DB::beginTransaction();
        try {
            $announcement = $dto->getAnnouncement();
            $announcement->title = $dto->getTitle();
            $announcement->description = $dto->getDescription();
            $announcement->status = $dto->getStatus();
            if ($dto->getFile() !== null) {
                File::delete('storage/announcements/' . $dto->getAnnouncement()->file);
                $image = $dto->getFile();
                $filename = Str::random(4) . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/announcements/', $filename);
                $announcement->file = $filename;
                $announcement->path = url('storage/announcements/' . $filename);
            }
            $announcement->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $announcement;
    }
}
