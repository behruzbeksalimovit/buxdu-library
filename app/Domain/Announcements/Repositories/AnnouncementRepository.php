<?php
namespace App\Domain\Announcements\Repositories;
use App\Domain\Announcements\Models\Announcement;

class AnnouncementRepository
{
    public function paginate()
    {
        return Announcement::query()
            ->orderByDesc('id')
            ->paginate();
    }

    public function getAll()
    {
        return Announcement::query()
            ->orderByDesc('id')
            ->get();
    }

    public function getNewsPaginate()
    {
        return Announcement::query()
            ->where('status','news')
            ->orderByDesc('id')
            ->paginate();
    }

    public function getAnnouncementPaginate()
    {
        return Announcement::query()
            ->where('status','announcement')
            ->orderByDesc('id')
            ->paginate();
    }
}
