<?php
namespace App\Domain\Announcements\DTO;
use App\Domain\Announcements\Models\Announcement;
use Illuminate\Http\UploadedFile;

class UpdateAnnouncementDTO
{
    private string $title;
    private ?string $description = null;
    private ?UploadedFile $file = null;
    private string $status;
    private Announcement $announcement;

    public static function fromArray(array $data)
    {
        $dto =new self();
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description'] ?? null);
        $dto->setFile($data['file'] ?? null);
        $dto->setStatus($data['status']);
        $dto->setAnnouncement($data['announcement']);

        return $dto;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Announcement
     */
    public function getAnnouncement(): Announcement
    {
        return $this->announcement;
    }

    /**
     * @param Announcement $announcement
     */
    public function setAnnouncement(Announcement $announcement): void
    {
        $this->announcement = $announcement;
    }
}
