<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use JetBrains\PhpStorm\ArrayShape;

class ElectronsFilter extends AbstractFilter
{
    public const TITLE = 'title';

    public const AUTHOR = 'author';

    public const CATEGORY_ID = 'category_id';

    /**
     * @return array[]
     */
    #[ArrayShape([self::TITLE => "array", self::AUTHOR => "array", self::CATEGORY_ID => "array"])] protected function getCallbacks(): array
    {
        return [
            self::TITLE => [$this, 'title'],
            self::AUTHOR => [$this, 'author'],
            self::CATEGORY_ID => [$this, 'category_id']
        ];
    }

    public function title(Builder $builder, $value): void
    {
        $builder->where('title', 'like', '%' . $value . '%');
    }

    public function author(Builder $builder, $value): void
    {
        $builder->where('author', 'like', '%' . $value . '%');
    }

    public function category_id(Builder $builder, $value): void
    {
        $builder->where('category_id', '=', $value);
    }
}
