<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BasicAuthForApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $authorization = $request->header('Authorization');

        if ($authorization) {
            $credentials = explode(':', base64_decode(substr($authorization, 6)), 2);

            $username = $credentials[0];
            $password = $credentials[1];

            // Custom logic to validate credentials
            if ($this->isValidCredentials($username, $password)) {
                return $next($request);
            }
        }

        return response('Unauthorized.', 401);
    }

    protected function isValidCredentials($username, $password)
    {
        // Custom logic to validate credentials
        // Example: Check against database or other authentication service
        return ($username === 'admin' && $password === 'apisecretapi');
    }
}
