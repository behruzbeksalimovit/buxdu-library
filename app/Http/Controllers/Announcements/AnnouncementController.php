<?php

namespace App\Http\Controllers\Announcements;

use App\Domain\Announcements\Actions\StoreAnnouncementAction;
use App\Domain\Announcements\Actions\UpdateAnnouncementAction;
use App\Domain\Announcements\DTO\StoreAnnouncementDTO;
use App\Domain\Announcements\DTO\UpdateAnnouncementDTO;
use App\Domain\Announcements\Models\Announcement;
use App\Domain\Announcements\Repositories\AnnouncementRepository;
use App\Domain\Announcements\Requests\StoreAnnouncementRequest;
use App\Domain\Announcements\Requests\UpdateAnnouncementRequest;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;

class AnnouncementController extends Controller
{
    /**
     * @var mixed|AnnouncementRepository
     */
    public mixed $announcements;

    /**
     * @param AnnouncementRepository $announcementRepository
     */
    public function __construct(AnnouncementRepository $announcementRepository)
    {
        $this->announcements = $announcementRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.announcements.index', [
            'announcements' => $this->announcements->paginate()
        ]);
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        return view('dashboard.announcements.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAnnouncementRequest $request, StoreAnnouncementAction $action)
    {

        try {
            $dto = StoreAnnouncementDTO::fromArray($request->validated());
            $action->execute($dto);
            return redirect()->route('announcements.index');
        } catch (Exception $exception) {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Announcement $announcements)
    {
        return view('dashboard.announcements.show', [
            'new' => $announcements
        ]);
    }

    /**
     * @param Announcement $announcement
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(Announcement $announcement)
    {
        return view('dashboard.announcements.edit', [
            'announcement' => $announcement
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAnnouncementRequest $request, Announcement $announcement, UpdateAnnouncementAction $action)
    {
        try {
            $request->validated();
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage()
                ]);
        }

        try {
            $request->merge([
                'announcement' => $announcement
            ]);
            $dto = UpdateAnnouncementDTO::fromArray($request->all());
            $action->execute($dto);

            return redirect()->route('announcements.index');
        } catch (Exception $exception) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Announcement $announcement)
    {
        File::delete('storage/announcements/' . $announcement->file);
        $announcement->delete();

        return redirect()->back();
    }

//--------------------------------------------------------------- API ----------------------------------------------------------------

//--------------------------------------------------------------- E'lonlar TYPE=1 ----------------------------------------------------------------

    public function getNews()
    {
        return $this->announcements->getNewsPaginate();
    }
//--------------------------------------------------------------- End e'lonlar  -------------------------------------------------------------------

//--------------------------------------------------------------- Yangiliklar TYPE = 2  -----------------------------------------------------------

    public function getAnnouncement()
    {
        return $this->announcements->getAnnouncementPaginate();
    }

//--------------------------------------------------------------- Yangiliklar TYPE = 2  -----------------------------------------------------------
}
