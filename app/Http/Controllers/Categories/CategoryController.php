<?php

namespace App\Http\Controllers\Categories;

use App\Domain\Categories\Actions\StoreCategoryAction;
use App\Domain\Categories\Actions\UpdateCategoryAction;
use App\Domain\Categories\DTO\StoreCategoryDTO;
use App\Domain\Categories\DTO\UpdateCategoryDTO;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Repositories\CategoryRepository;
use App\Domain\Categories\Requests\StoreCategoryRequest;
use App\Domain\Categories\Requests\UpdateCategoryRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Categories\CategoryResource;
use Exception;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var mixed|CategoryRepository
     */
    public mixed $categories;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categories = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.categories.index', [
            'categories' => $this->categories->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.categories.create', [
            'categories' => $this->categories->getAllParentNull()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request, StoreCategoryAction $action)
    {
        $request->validated();
        try {
            $dto = StoreCategoryDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        return view('dashboard.categories.edit', [
            'categories' => $this->categories->getAllParentNull(),
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category, UpdateCategoryAction $action)
    {
        $request->validated();
        $request->merge([
            'category' => $category
        ]);
        try {
            $dto = UpdateCategoryDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->back();
    }


//-----------------------------------------------------------------------------------------   API   ------------------------------------------------------------------------------

    public function getAllParentNullCategory()
    {
        return CategoryResource::collection($this->categories->getAllParentNull());
    }

//-----------------------------------------------------------------------------------------   API   ------------------------------------------------------------------------------
}
