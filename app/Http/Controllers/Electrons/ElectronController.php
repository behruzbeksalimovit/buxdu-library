<?php

namespace App\Http\Controllers\Electrons;

use App\Domain\Electrons\Actions\UpdateElectronAction;
use App\Domain\Electrons\DTO\UpdateElectronDTO;
use App\Domain\Categories\Repositories\CategoryRepository;
use App\Domain\Electrons\Actions\StoreElectronAction;
use App\Domain\Electrons\DTO\StoreElectronDTO;
use App\Domain\Electrons\Models\Electron;
use App\Domain\Electrons\Repositories\ElectronRepository;
use App\Domain\Electrons\Requests\StoreElectronRequest;
use App\Domain\Electrons\Requests\UpdateElectronRequest;
use App\Filters\ElectronsFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Filters\ElectronFilterRequest;
use App\Http\Resources\Api\Electrons\ElectronResource;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\File;

class ElectronController extends Controller
{
    /**
     * @var mixed|electronRepository
     */
    public mixed $electrons;

    /**
     * @var mixed|CategoryRepository
     */
    public mixed $categories;

    /**
     * @param ElectronRepository $electronRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(ElectronRepository $electronRepository, CategoryRepository $categoryRepository)
    {
        $this->electrons = $electronRepository;
        $this->categories = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.electrons.index', [
            'electrons' => $this->electrons->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.electrons.create', [
            'categories' => $this->categories->getAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreElectronRequest $request, StoreElectronAction $action)
    {
        $request->validated();
        try {
            $dto = StoreElectronDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('electrons.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Electron $electron)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Electron $electron)
    {
        return view('dashboard.electrons.edit', [
            'electron' => $electron,
            'categories' => $this->categories->getAll()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateElectronRequest $request, Electron $electron, UpdateElectronAction $action)
    {
        $request->merge([
            'electron' => $electron
        ]);
        $request->validated();
        try {
            $dto = UpdateElectronDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('electrons.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(electron $electron)
    {
        File::delete('storage/electrons/images/' . $electron->image);
        File::delete('storage/electrons/files/' . $electron->file);
        $electron->delete();

        return redirect()->back();
    }


    //------------------------------------------------------------------------  API  ---------------------------------------------------------------------------------------

    /**
     * @return AnonymousResourceCollection
     * @throws BindingResolutionException
     */
    public function getAllBook(ElectronFilterRequest $request)
    {
        $filter = app()->make(ElectronsFilter::class, ['queryParams' => array_filter($request->validated())]);
        return ElectronResource::collection($this->electrons->getAllBookPaginate($filter));
    }

    /**
     * @param $book_id
     * @return ElectronResource
     */
    public function showBook($book_id)
    {
        return new ElectronResource($this->electrons->showBook($book_id));
    }

//------------------------------------------------------------------------  API  ---------------------------------------------------------------------------------------

}
