<?php

namespace App\Http\Controllers\Genres;

use App\Domain\Genres\Actions\StoreGenreAction;
use App\Domain\Genres\Actions\UpdateGenreAction;
use App\Domain\Genres\DTO\StoreGenreDTO;
use App\Domain\Genres\DTO\UpdateGenreDTO;
use App\Domain\Genres\Repositories\GenreRepository;
use App\Domain\Genres\Requests\StoreGenreRequest;
use App\Domain\Genres\Requests\UpdateGenreRequest;
use App\Domain\Genres\Models\Genre;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * @var mixed|GenreRepository
     */
    public mixed $genres;

    /**
     * @param GenreRepository $genreRepository
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genres = $genreRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.genres.index', [
            'genres' => $this->genres->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.genres.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGenreRequest $request, StoreGenreAction $action)
    {
        $request->validated();
        try {
            $dto = StoreGenreDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('genres.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Genre $genre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Genre $genre)
    {
        return view('dashboard.genres.edit', [
            'genre' => $genre
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGenreRequest $request, Genre $genre, UpdateGenreAction $action)
    {
        $request->merge([
            'genre' => $genre
        ]);
        $request->validated();
        try {
            $dto = UpdateGenreDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('genres.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Genre $genre)
    {
        $genre->delete();
        return redirect()->back();
    }
}
