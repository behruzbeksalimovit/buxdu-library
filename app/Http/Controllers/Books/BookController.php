<?php

namespace App\Http\Controllers\Books;

use App\Domain\Books\Actions\StoreBookAction;
use App\Domain\Books\Actions\UpdateBookAction;
use App\Domain\Books\DTO\StoreBookDTO;
use App\Domain\Books\DTO\UpdateBookDTO;
use App\Domain\Books\Repositories\BookRepository;
use App\Domain\Books\Requests\StoreBookRequest;
use App\Domain\Books\Requests\UpdateBookRequest;
use App\Domain\Categories\Repositories\CategoryRepository;
use App\Domain\Genres\Repositories\GenreRepository;
use App\Filters\BooksFilter;
use App\Http\Controllers\Controller;
use App\Domain\Books\Models\Book;
use App\Http\Requests\Filters\BookFilterRequest;
use App\Http\Resources\Api\Books\BookResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class BookController extends Controller
{
    /**
     * @var mixed|BookRepository
     */
    public mixed $books;

    /**
     * @var mixed|CategoryRepository
     */
    public mixed $categories;

    /**
     * @var mixed|GenreRepository
     */
    public mixed $genres;

    /**
     * @param BookRepository $bookRepository
     * @param CategoryRepository $categoryRepository
     * @param GenreRepository $genreRepository
     */
    public function __construct(BookRepository $bookRepository, CategoryRepository $categoryRepository, GenreRepository $genreRepository)
    {
        $this->books = $bookRepository;
        $this->categories = $categoryRepository;
        $this->genres = $genreRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.books.index', [
            'books' => $this->books->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.books.create', [
            'categories' => $this->categories->getAll(),
            'genres' => $this->genres->getAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBookRequest $request, StoreBookAction $action)
    {
        $request->validated();
        try {
            $dto = StoreBookDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Book $book)
    {
        return view('dashboard.books.edit', [
            'book' => $book,
            'categories' => $this->categories->getAll(),
            'genres' => $this->genres->getAll(),
            'selected_genres' => $book->genres->pluck('pivot.genre_id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBookRequest $request, Book $book, UpdateBookAction $action)
    {
        $request->merge([
            'book' => $book
        ]);
        $request->validated();
        try {
            $dto = UpdateBookDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        File::delete('storage/books/images/' . $book->image);
        File::delete('storage/books/files/' . $book->file);
        $book->delete();

        return redirect()->back();
    }


//------------------------------------------------------------------------  API  ---------------------------------------------------------------------------------------

    /**
     * @return AnonymousResourceCollection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getAllBook(BookFilterRequest $request)
    {
        $filter = app()->make(BooksFilter::class, ['queryParams' => array_filter($request->validated())]);
        return BookResource::collection($this->books->getAllBookPaginate($filter));
    }

    /**
     * @return BookResource
     */
    public function showBook($book_id)
    {
        return new BookResource($this->books->showBook($book_id));
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function showForCategoryAllBooks($category_id)
    {
        return BookResource::collection($this->books->showForCategoryAllBooks($category_id));
    }

//------------------------------------------------------------------------  API  ---------------------------------------------------------------------------------------
}
