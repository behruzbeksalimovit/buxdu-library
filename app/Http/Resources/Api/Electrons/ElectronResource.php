<?php

namespace App\Http\Resources\Api\Electrons;

use App\Http\Resources\Api\Categories\CategoryResource;
use App\Http\Resources\Api\Genres\GenreResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ElectronResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
            'description' => $this->description,
            'year_publication' => $this->year_publication,
            'page_count' => $this->page_count,
            'image' => url('storage/electrons/images/'.$this->image),
            'file' => url('storage/electrons/files/'.$this->file),
            'categories' => new CategoryResource($this->category)
        ];
    }
}
