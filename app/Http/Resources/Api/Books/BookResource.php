<?php

namespace App\Http\Resources\Api\Books;

use App\Http\Resources\Api\Categories\CategoryResource;
use App\Http\Resources\Api\Genres\GenreResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
            'description' => $this->description,
            'image' => url('storage/books/images/'.$this->image),
            'file' => url('storage/books/files/'.$this->file),
            'genres' => GenreResource::collection($this->genres),
            'categories' => new CategoryResource($this->category)
        ];
    }
}
