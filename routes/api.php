<?php

use App\Http\Controllers\Announcements\AnnouncementController;
use App\Http\Controllers\Books\BookController;
use App\Http\Controllers\Categories\CategoryController;
use App\Http\Controllers\Electrons\ElectronController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors']], function () {
    Route::get('categories',[CategoryController::class,'getAllParentNullCategory']);
    Route::get('books',[BookController::class,'getAllBook']);
    Route::get('electron',[ElectronController::class,'getAllBook']);
    Route::get('book/{book_id}',[BookController::class,'showBook']);
    Route::get('electron/{electron_id}',[ElectronController::class,'showBook']);
    Route::get('category/{category_id}/books',[BookController::class,'showForCategoryAllBooks']);
    Route::get('news',[AnnouncementController::class,'getNews']);
    Route::get('announcement',[AnnouncementController::class,'getAnnouncement']);
});
