<?php

use App\Http\Controllers\Announcements\AnnouncementController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Books\BookController;
use App\Http\Controllers\Categories\CategoryController;
use App\Http\Controllers\Electrons\ElectronController;
use App\Http\Controllers\Genres\GenreController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'auth'])->name('auth.login');
Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');

//---------------------------------------------------------------  ADMIN START -------------------------------------------------------------------------------------------------
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth:sanctum']], function () {
    Route::get('index', [HomeController::class, 'index'])->name('admin.home');
    Route::resource('categories', CategoryController::class);
    Route::resource('genres', GenreController::class);
    Route::resource('books', BookController::class);
    Route::resource('electrons', ElectronController::class);
    Route::resource('announcements', AnnouncementController::class);
});
//---------------------------------------------------------------  ADMIN END -----------------------------------------------------------------------------------------------------

