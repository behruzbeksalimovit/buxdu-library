@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-flex align-content-center justify-content-between">
            <h5 class="mb-0">Kitoblar ro'yxati</h5>
            <div>
                <a href="{{ route('books.create') }}" class="btn btn-sm btn-success">Qo'shish</a>
                <button type="button" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Author</th>
                        <th>Kategoriya</th>
                        <th>Janr</th>
                        <th>Title</th>
                        <th>Harakatlar</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td>{{ ($books->currentPage()-1) * $books->perPage() + $loop->index+1 }}</td>
                            <td>{{ $book->author }}</td>
                            <td>{{ $book->category->name }}</td>
                            <td>
                                @foreach($book->genres as $genre)
                                    <span class="badge bg-success">{{ $genre->name }}</span>
                                @endforeach
                            </td>
                            <td>{{ \Illuminate\Support\Str::limit($book->title,50,"...") }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('storage/books/images/'.$book->image) }}"
                                         style="margin-right: 10px;" class="w-40px h-40px rounded-pill"
                                         alt="book image">
                                    <a target="_blank" class="btn btn-sm btn-flat-info" style="margin-right: 10px;"
                                       href="{{ asset('storage/books/files/'.$book->file) }}">
                                        <i class="ph-book"></i>
                                    </a>
                                    <a href="{{ route('books.edit',$book) }}" class="btn btn-sm btn-info"
                                       style="margin-right: 10px;"><i class="ph-pencil"></i></a>
                                    <form action="{{ route('books.destroy',$book) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Siz ushbu kitobni o\'chirmoqchimisiz?');"
                                                type="submit" class="btn btn-sm btn-danger"><i class="ph-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $books->links() }}
        </div>
    </div>
@endsection
