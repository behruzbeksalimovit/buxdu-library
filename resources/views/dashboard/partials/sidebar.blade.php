<div class="sidebar sidebar-dark sidebar-main sidebar-expand-lg">

    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Sidebar header -->
        <div class="sidebar-section">
            <div class="sidebar-section-body d-flex justify-content-center">
                <h5 class="sidebar-resize-hide flex-grow-1 my-auto">Navigation</h5>

                <div>
                    <button type="button" class="btn btn-flat-white btn-icon btn-sm rounded-pill border-transparent sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
                        <i class="ph-arrows-left-right"></i>
                    </button>

                    <button type="button" class="btn btn-flat-white btn-icon btn-sm rounded-pill border-transparent sidebar-mobile-main-toggle d-lg-none">
                        <i class="ph-x"></i>
                    </button>
                </div>
            </div>
        </div>
        <!-- /sidebar header -->


        <!-- Main navigation -->
        <div class="sidebar-section">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header pt-0">
                    <div class="text-uppercase fs-sm lh-sm opacity-50 sidebar-resize-hide">Main</div>
                    <i class="ph-dots-three sidebar-resize-show"></i>
                </li>

{{--                @if(\Illuminate\Support\Facades\Auth::user()->role_id == 1) {{--    ADMIN ROLE     --}}
                    <li class="nav-item">
                        <a href="{{ route('genres.index') }}" class="nav-link">
                            <i class="ph-article"></i>
                            <span>Janrlar ro'yxati</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('categories.index') }}" class="nav-link">
                            <i class="ph-list-bullets"></i>
                            <span>Kategoriyalar</span>
                        </a>
                    </li>

                <li class="nav-item">
                    <a href="{{ route('books.index') }}" class="nav-link">
                        <i class="ph-book"></i>
                        <span>Kitoblar</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('announcements.index') }}" class="nav-link">
                        <i class="ph-book"></i>
                        <span>Yangiliklar</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('electrons.index') }}" class="nav-link">
                        <i class="ph-book"></i>
                        <span>Elektron fayllar</span>
                    </a>
                </li>

{{--                    <li class="nav-item nav-item-submenu">--}}
{{--                        <a href="#" class="nav-link">--}}
{{--                            <i class="ph-users"></i>--}}
{{--                            <span>Kategoriyalar</span>--}}
{{--                        </a>--}}
{{--                        <ul class="nav-group-sub collapse">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{ route('categ.index') }}" class="nav-link">Ichki formalar</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{ route('external.index') }}" class="nav-link">Tashqi formalar</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                @endif--}}
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
