@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-header">
                <h5 class="mb-0">Kategoriya qo'shish</h5>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif
            </div>

            <div class="card-body row">
                <div class="mb-3 col-md-12">
                    <label for="parent_id" class="form-label">Asosiy kategoriya <span class="text-danger fw-bold">(Majburiy emas!)</span></label>
                    <select class="form-control" name="parent_id" id="parent_id">
                        <option value="">Asosiy kategoriyani tanlang!</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3 col-md-12">
                    <label for="name" class="form-label">Kategoriya nomi</label>
                    <input type="text" value="{{ old('name') }}" id="name" class="form-control" name="name" placeholder="Kategoriya nomi">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary w-100">Saqlash <i class="ph-paper-plane-tilt ms-2"></i>
                </button>
            </div>
        </form>
    </div>
@endsection
