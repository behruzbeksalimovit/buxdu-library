@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-flex align-content-center justify-content-between">
            <h5 class="mb-0">Kategoriyalar ro'yxati</h5>
            <div>
                <a href="{{ route('categories.create') }}" class="btn btn-sm btn-success">Qo'shish</a>
                <button type="button" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Kategoriya nomi</th>
                        <th>Harakat</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ ($categories->currentPage()-1)*$categories->perPage() + $loop->index+1 }}</td>
                            <td>{{ $category->name }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <a href="{{ route('categories.edit',$category) }}" class="btn btn-sm btn-info" style="margin-right: 10px;"><i class="ph-pencil"></i></a>
                                    <form action="{{ route('categories.destroy',$category) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Siz ushbu kategoriyani o\'chirmoqchimisiz? (Agar bu asosiy kategoriya bo\'lsa bunga bog\'langan barcha sub kategoriyalar o\'chiriladi!!!)');" type="submit" class="btn btn-sm btn-danger"><i class="ph-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $categories->links() }}
        </div>
    </div>
@endsection
