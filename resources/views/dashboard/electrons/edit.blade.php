@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <form action="{{ route('electrons.update',$electron) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-header">
                <h5 class="mb-0">Elektron fayllar tahrirlash</h5>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif
            </div>

            <div class="card-body row">
                <div class="col-md-5 mb-3">
                    <label for="category_id" class="form-label">Kategoriya</label>
                    <select id="category_id" data-placeholder="Kategoriyani tanlang!" class="select form-control"
                            name="category_id">
                        @foreach($categories as $category)
                            @if($category->id == $electron->category_id)
                                <option selected value="{{ $category->id }}">{{$category->name}}</option>
                            @else
                                <option value="{{ $category->id }}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="author" class="form-label">Avtor F.I.O</label>
                    <input name="author" id="author" type="text" class="form-control" placeholder="Salimov Bekhruz" value="{{ $electron->author }}">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="year_publication" class="form-label">Nashr yili</label>
                    <input name="year_publication" id="year_publication" type="number" class="form-control"
                           placeholder="1992" value="{{ $electron->year_publication }}">
                </div>
                <div class="col-md-2 mb-3">
                    <label for="page_count" class="form-label">Sahifalar soni</label>
                    <input name="page_count" id="page_count" type="number" class="form-control" placeholder="300" value="{{ $electron->page_count }}">
                </div>
                <div class="mb-3 col-md-6">
                    <label for="title" class="form-label">Title</label>
                    <textarea class="form-control" rows="10" id="title" name="title"
                              placeholder="Title...">{{ $electron->title }}</textarea>
                </div>
                <div class="mb-3 col-md-6">
                    <label for="description" class="form-label">Anotatsiya</label>
                    <textarea class="form-control" rows="10" id="description" name="description"
                              placeholder="Anotatsiya...">{{ $electron->description }}</textarea>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="image" class="form-label">Kitob rasmi <span class="text-danger fw-bold">(format:png, jpg, jpeg  max: 10MB)</span></label>
                    <input style="margin-bottom: 10px;" name="image" id="image" type="file" class="form-control">
                    <img src="{{ asset('storage/electrons/images/'.$electron->image) }}" style="margin-right: 10px;"
                         class="w-40px h-40px rounded-pill" alt="book image">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="file" class="form-label">Kitob fayli <span class="text-danger fw-bold">(format:pdf,zip,rar  max: 300MB)</span></label>
                    <input style="margin-bottom: 10px;" name="file" id="file" type="file" class="form-control">
                    <a target="_blank" class="btn btn-sm btn-flat-info" style="margin-right: 10px;"
                       href="{{ asset('storage/electrons/files/'.$electron->file) }}">
                        <i class="ph-book"></i>
                    </a>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary w-100">Saqlash <i class="ph-paper-plane-tilt ms-2"></i>
                </button>
            </div>
        </form>
    </div>
@endsection
@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.select').select2();
        });
    </script>
@endpush
