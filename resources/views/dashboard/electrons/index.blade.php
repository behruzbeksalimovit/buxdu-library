@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-flex align-content-center justify-content-between">
            <h5 class="mb-0">Elektron fayllar ro'yxati</h5>
            <div>
                <a href="{{ route('electrons.create') }}" class="btn btn-sm btn-success">Qo'shish</a>
                <button type="button" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Author</th>
                        <th>Kategoriya</th>
                        <th>Title</th>
                        <th>Harakatlar</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($electrons as $electron)
                        <tr>
                            <td>{{ ($electrons->currentPage()-1) * $electrons->perPage() + $loop->index+1 }}</td>
                            <td>{{ $electron->author }}</td>
                            <td>{{ $electron->category->name }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($electron->title,50,"...") }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('storage/electrons/images/'.$electron->image) }}"
                                         style="margin-right: 10px;" class="w-40px h-40px rounded-pill"
                                         alt="book image">
                                    <a target="_blank" class="btn btn-sm btn-flat-info" style="margin-right: 10px;"
                                       href="{{ asset('storage/electrons/files/'.$electron->file) }}">
                                        <i class="ph-book"></i>
                                    </a>
                                    <a href="{{ route('electrons.edit',$electron) }}" class="btn btn-sm btn-info"
                                       style="margin-right: 10px;"><i class="ph-pencil"></i></a>
                                    <form action="{{ route('electrons.destroy',$electron) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Siz ushbu faylni o\'chirmoqchimisiz?');"
                                                type="submit" class="btn btn-sm btn-danger"><i class="ph-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $electrons->links() }}
        </div>
    </div>
@endsection
