@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-flex align-content-center justify-content-between">
            <h5 class="mb-0">Janrlar ro'yxati</h5>
            <div>
                <a href="{{ route('genres.create') }}" class="btn btn-sm btn-success">Qo'shish</a>
                <button type="button" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Janr nomi</th>
                        <th>Harakat</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($genres as $genre)
                        <tr>
                            <td>{{ ($genres->currentPage()-1)*$genres->perPage() + $loop->index+1 }}</td>
                            <td>{{ $genre->name }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <a href="{{ route('genres.edit',$genre) }}" class="btn btn-sm btn-info" style="margin-right: 10px;"><i class="ph-pencil"></i></a>
                                    <form action="{{ route('genres.destroy',$genre) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Siz ushbu janrni o\'chirmoqchimisiz?');" type="submit" class="btn btn-sm btn-danger"><i class="ph-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $genres->links() }}
        </div>
    </div>
@endsection
