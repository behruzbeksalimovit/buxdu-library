@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <form action="{{ route('genres.update',$genre) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-header">
                <h5 class="mb-0">Janrni tahrirlash</h5>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif
            </div>

            <div class="card-body row">
                <div class="mb-3 col-md-12">
                    <label for="name" class="form-label">Janr nomi</label>
                    <input type="text" value="{{ $genre->name }}" id="name" class="form-control" name="name"
                           placeholder="Janr nomi">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary w-100">Saqlash <i class="ph-paper-plane-tilt ms-2"></i>
                </button>
            </div>
        </form>
    </div>
@endsection
