@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-flex align-content-center justify-content-between">
            <h5 class="mb-0">Yangiliklar va e'lonlar ro'yxati</h5>
            <div>
                <a href="{{ route('announcements.create') }}" class="btn btn-sm btn-success">Qo'shish</a>
                <button type="button" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Sarlavha</th>
                        <th>Tavsif</th>
                        <th>Status</th>
                        <th>Harakatlar</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($announcements as $announcement)
                        <tr>
                            <td>{{ ($announcements->currentPage()-1)*$announcements->perPage() + $loop->index+1 }}</td>
                            <td>{{ $announcement->title }}</td>
                            <td>{{ $announcement->description ?? '' }}</td>
                            <td>{{ $announcement->status }}</td>
                            <td>
                                @if($announcement->type == 1)
                                    <span class="badge bg-warning">E'lon</span>
                                @else
                                    <span class="badge bg-success">Yangilik</span>
                                @endif
                            </td>
                            <td>
                                <div class="d-flex align-items-center">

                                    <a href="{{ route('announcements.edit',$announcement) }}" class="btn btn-sm btn-info"
                                       style="margin-right: 10px;"><i class="ph-pencil"></i></a>
                                    <form action="{{ route('announcements.destroy',$announcement) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button
                                            onclick="return confirm('Siz ushbu yangilikni o\'chirmoqchimisiz?');"
                                            type="submit" class="btn btn-sm btn-danger"><i class="ph-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $announcements->links() }}
        </div>
    </div>
@endsection
