@extends('dashboard.layouts.master')
@section('content')
    <div class="card">
        <form action="{{ route('announcements.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-header">
                <h5 class="mb-0">Yangilik va E'lon qo'shish</h5>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif
            </div>

            <div class="card-body row">
                <div class="mb-3 col-md-4">
                    <label for="title" class="form-label">Sarlavha<span class="text-danger fw-bold">*</span></label>
                    <textarea type="text" id="title" class="form-control" rows="5" cols="3"
                              name="title"
                              placeholder="Sarlavha">{{ old('title') }}</textarea>
                </div>

                <div class="mb-3 col-md-4">
                    <label for="description" class="form-label">Tavsif<span class="text-danger fw-bold">*</span></label>
                    <textarea name="description" id="description" rows="10" cols="3" class="form-control"
                              placeholder="Tavsifni kiriting...">{{ old('description') }}</textarea>
                </div>


                <div class="mb-3 col-md-4">
                    <label for="file" class="form-label">Yangilik yoki e'lon rasmini joylashtiring! <span class="text-danger">(png,jpeg,jpg)</span></label>
                    <input type="file" name="file" class="form-control" id="file">
                </div>

                <div class="col-md-6 d-flex align-items-center justify-content-start">

                    <div class="form-check form-switch me-4">
                        <input type="radio" name="status" class="form-check-input form-check-input-success" id="type_1"
                               value="announcement" checked>
                        <label class="form-check-label" for="type_1">E'lon <span class="text-danger fw-bold">*</span></label>
                    </div>

                    <div class="form-check form-switch">
                        <input type="radio" name="status" class="form-check-input form-check-input-success" id="type_2"
                               value="news">
                        <label class="form-check-label" for="type_2">Yangilik <span class="text-danger fw-bold">*</span></label>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary w-100">Saqlash <i class="ph-paper-plane-tilt ms-2"></i>
                </button>
            </div>
        </form>
    </div>
@endsection
